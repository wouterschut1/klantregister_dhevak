
<!DOCTYPE html>
<html>
<head>

<!-- https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/sandstone/bootstrap.min.css -->

<!-- Bootstrap Overlay thema -->
<!-- <link rel="stylesheet" type="text/css" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/sandstone/bootstrap.min.css') }}"> -->

<!-- Extra CSS file  -->
<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/font-face.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/sb-admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/sb-admin.min.css') }}">




<!-- Lato font - (WSDEV Font) -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

<!-- Bootstrap 1/2 -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<!-- Bootstrap 2/2 -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


<!-- </head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-brand">Klant registratie systeem<br> <h5>Dhevak</h5></div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="/shares">Overizcht |<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/shares/create">Klant registreren |</a>
        </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Zoek in contacten">
        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Zoeken</button>
        </form>
    </div>
    </nav> -->