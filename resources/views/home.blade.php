@include('inc.header') @extends('layout')

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dhèvak CRM</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>
<!-- This demo uses the Chart.js graphing library and Moment.js to do date
     formatting and manipulation. -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<!-- Include the ViewSelector2 component script. -->
<script src="/public/javascript/embed-api/components/view-selector2.js"></script>

<!-- Include the DateRangeSelector component script. -->
<script src="/public/javascript/embed-api/components/date-range-selector.js"></script>

<!-- Include the ActiveUsers component script. -->
<script src="/public/javascript/embed-api/components/active-users.js"></script>

<!-- Include the CSS that styles the charts. -->
<link rel="stylesheet" href="/public/css/chartjs-visualizations.css">
 <div id="embed-api-auth-container"></div>
                                    <div id="view-selector-container"></div>
                                    <div id="view-name"></div>
                                    <div id="active-users-container"></div>
                                    

</head>

<body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top" style="background-color:#6CB52D!important">

        <a class="navbar-brand mr-1" href="/home">Dhèvak CRM</a>
        <br>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projecten">
                    <i class="fas fa-plus"></i>
                    <span>Projecten</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/shares">
                    <i class="fas fa-plus"></i>
                    <span> Klanten</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-google-drive"></i>
                    <span> Google Drive</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-trello"></i>
                    <span> Trello</span></a>
            </li>
        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#" style="color:#6CB52D">Dashboard</a>
                    </li>
                </ol>

                <div class="container">
                    <div class="row">
                        <div class="col">
                            <style>
                                .button303 {
                                    border-radius: 4px;
                                    background-color: #e6e8ed;
                                    border: none;
                                    color: #FFFFFF;
                                    text-align: center;
                                    font-size: 28px;
                                    padding: 20px;
                                    width: 200px;
                                    transition: all 0.5s;
                                    cursor: pointer;
                                    margin: 5px;
                                }
                                
                                .button303 span {
                                    cursor: pointer;
                                    display: inline-block;
                                    position: relative;
                                    transition: 0.5s;
                                }
                                
                                .button303 span:after {
                                    content: '\00bb';
                                    position: absolute;
                                    opacity: 0;
                                    top: 0;
                                    right: -20px;
                                    transition: 0.5s;
                                }
                                
                                .button303:hover span {
                                    padding-right: 25px;
                                }
                                
                                .button303:hover span:after {
                                    opacity: 1;
                                    right: 0;
                                }
                                
                                .button202 {
                                    border-radius: 4px;
                                    background-color: #6CB52D;
                                    border: none;
                                    color: #FFFFFF;
                                    text-align: center;
                                    font-size: 28px;
                                    padding: 20px;
                                    width: 200px;
                                    transition: all 0.5s;
                                    cursor: pointer;
                                    margin: 5px;
                                }
                                
                                .button202 span {
                                    cursor: pointer;
                                    display: inline-block;
                                    position: relative;
                                    transition: 0.5s;
                                }
                                
                                .button202 span:after {
                                    content: '\00bb';
                                    position: absolute;
                                    opacity: 0;
                                    top: 0;
                                    right: -20px;
                                    transition: 0.5s;
                                }
                                
                                .button202:hover span {
                                    padding-right: 25px;
                                }
                                
                                .button202:hover span:after {
                                    opacity: 1;
                                    right: 0;
                                }
                                
                                .button101 {
                                    border-radius: 4px;
                                    background-color: #212528;
                                    border: none;
                                    color: #FFFFFF;
                                    text-align: center;
                                    font-size: 28px;
                                    padding: 20px;
                                    width: 200px;
                                    transition: all 0.5s;
                                    cursor: pointer;
                                    margin: 5px;
                                }
                                
                                .button101 span {
                                    cursor: pointer;
                                    display: inline-block;
                                    position: relative;
                                    transition: 0.5s;
                                }
                                
                                .button101 span:after {
                                    content: '\00bb';
                                    position: absolute;
                                    opacity: 0;
                                    top: 0;
                                    right: -20px;
                                    transition: 0.5s;
                                }
                                
                                .button101:hover span {
                                    padding-right: 25px;
                                }
                                
                                .button101:hover span:after {
                                    opacity: 1;
                                    right: 0;
                                }
                            </style>

                        </div>
                        <div class="col">

                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h2>Projecten</h2>

                                <a href="/projecten/create">
                                    <button class="button202"><span>Toevoegen </span></button>
                                </a>
                                <a href="/projecten">
                                    <button class="button202"><span>Overzicht </span></button>
                                </a>

                            </div>
                            <div class="col">
                                <h2>Klanten</h2>
                                <a href="/shares/create">
                                    <button class="button101"><span>Toevoegen </span></button>
                                </a>
                                <a href="/shares">
                                    <button class="button101"><span>Overzicht </span></button>
                                </a>
                            </div>
                        </div>
                        <div style="padding:25px;"></div>
                        <div class="row">
                            <div class="col">
                                <h2>Externe links</h2>

                                <a href="https://www.google.com/drive/">
                                    <button class="button303"><span><img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png" style="width:65%"> </span></button>
                                </a>
                                <a href="https://trello.com">
                                    <button class="button303"><span><img src="https://d2k1ftgv7pobq7.cloudfront.net/meta/p/res/images/c13d1cd96a2cff30f0460a5e1860c5ea/header-logo-blue.svg" style="width:65%"> </span></button>
                                </a>
                            </div>
                            <div class="col">
                                <h2>Statistieken</h2>
                                <a href="https://www.google.com/drive/">
                                    <button class="button303"><span><img src="https://developers.google.com/analytics/images/terms/logo_lockup_analytics_icon_horizontal_black_2x.png" style="width:100%"> </span></button>
                                </a>
                                <a href="https://www.google.com/drive/">
                                    <button class="button303"><span><img src="https://vignette.wikia.nocookie.net/logopedia/images/e/e7/Google_adsense.png/revision/latest?cb=20171221221111" style="width:100%"> </span></button>
                                </a>
                            </div>
        
</body>

</html>