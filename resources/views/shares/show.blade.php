@include('inc.header') @extends('layout')

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dhèvak CRM</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top" style="background-color:#6CB52D!important">

        <a class="navbar-brand mr-1" href="/home">Dhèvak CRM</a>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>

        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item ">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projecten">
                    <i class="fas fa-plus"></i>
                    <span>Projecten</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/shares">
                    <i class="fas fa-plus"></i>
                    <span> Klanten</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-google-drive"></i>
                    <span> Google Drive</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-trello"></i>
                    <span> Trello</span></a>
            </li>
        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/home" style="color:#6CB52D">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="/shares" style="color:#6CB52D">Klanten</a>
                    </li>
                    <li class="breadcrumb-item active">Preview</li>
                </ol>
                @section('content')
                <style>
                    .uper {
                        margin-top: 40px;
                    }
                </style>
                <div class="card uper">
                    <div class="card-header">
                        <h2>Algemene informatie</h2>
                    </div>
                    <div class="card-body">
                        @if ($errors->any()) @endif
                        <form method="post" action="{{ route('shares.show', $share->id) }}">
                            @method('PATCH') @csrf
                            <div class="form-group">
                                <label for="voornaam"><b>Voornaam:</b> {{ $share->voornaam }}</label>
                            </div>
                            <div class="form-group">
                                <label for="tussenvoegsel"><b>Tussenvoegsel:</b> {{ $share->tussenvoegsel }}</label>
                            </div>
                            <div class="form-group">
                                <label for="achternaam"><b>Achternaam:</b> {{ $share->achternaam }}</label>
                            </div>
                        </form>
                        <br>

                    </div>
                </div>
                <div class="card uper">
                    <div class="card-header">
                        <h3>Klantendata</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="klantnummer"><b>Klantnummer:</b> {{ $share->klantnummer }}</label>
                        </div>
                        <div class="form-group">
                            <label for="email"><b>E-mail adres:</b> {{ $share->email }}</label>
                        </div>
                        <div class="form-group">
                            <label for="bedrijfsnaam"><b>Bedrijfsnaam:</b> {{ $share->bedrijfsnaam }}</label>
                        </div>
                        <button class="btn btn-primary"><a href="/shares" style="color:white">sluit</a></button>
                        </form>
                        <br>

                    </div>
                </div>
                @endsection