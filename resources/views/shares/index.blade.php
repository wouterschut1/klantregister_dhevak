@include('inc.header') @extends('layout')

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dhèvak CRM</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top" style="background-color:#6CB52D!important">

        <a class="navbar-brand mr-1" href="/home">Dhèvak CRM</a>
        <br>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item ">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projecten">
                    <i class="fas fa-plus"></i>
                    <span>Projecten</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/shares">
                    <i class="fas fa-plus"></i>
                    <span> Klanten</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-google-drive"></i>
                    <span> Google Drive</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-trello"></i>
                    <span> Trello</span></a>
            </li>
        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="home" style="color:#6CB52D">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Klanten</li>
                </ol>

                <h2>Klanten</h2>
                <a href="/shares/create">
                    <button class="btn btn-secondary"><span> <i class="fas fa-plus"></i> Snel toevoegen </span></button>
                </a>
            </div>
            <div style="padding:15px"></div>

            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i> Klantoverzicht
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Bedrijf</th>
                                    <th>Klantnummer</th>
                                    <th>Bekijk</th>
                                    <th>Bewerk</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                @foreach($shares as $share )

                                <tr>
                                    <!-- <th>{{$share->id}}</td> -->
                                    <th>{{$share->voornaam}} {{$share->tussenvoegsel}} {{$share->achternaam}}</th>
                                    <th>{{$share->bedrijfsnaam}}</td>
                                        <th>{{$share->klantnummer}}</td>
                                            <th><a href="{{ route('shares.show',$share->id)}}" class="btn btn-info">Bekijk</a></th>
                                            <th><a href="{{ route('shares.edit',$share->id)}}" class="btn btn-warning">Edit</a></th>
                                            <th>
                                                <form action="{{ route('shares.destroy', $share->id)}}" method="post">
                                                    @csrf @method('DELETE')
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </th>
                                </tr>

                                @endforeach
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
            </div>

        </div>
        <!-- /.container-fluid -->
        <!-- DataTables Example -->
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy 2019 Dhèvak</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

</body>

</html>