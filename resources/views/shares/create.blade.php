@include('inc.header') @extends('layout')

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dhèvak CRM</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top" style="background-color:#6CB52D!important">

        <a class="navbar-brand mr-1" href="/home">Dhèvak CRM</a>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>

        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item ">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projecten">
                    <i class="fas fa-plus"></i>
                    <span>Projecten</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/shares">
                    <i class="fas fa-plus"></i>
                    <span> Klanten</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-google-drive"></i>
                    <span> Google Drive</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://drive.google.com/drive/folders/0Bzj6wbyOEKM0TlBLSTFJODFzbjg" target="_blank">
                    <i class="fab fa-trello"></i>
                    <span> Trello</span></a>
            </li>
        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#" style="color:#6CB52D">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Overview</li>
                </ol>

                @section('content')
                <style>
                    .uper {
                        margin-top: 40px;
                    }
                </style>
                <div class="card uper">
                    <div class="card-header">
                        <h2>Contact toevoegen</h2>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <br /> @endif
                        <form method="post" action="{{ route('shares.store') }}">
                            <div class="form-group">
                                {{ csrf_field() }}
                                <label for="voornaam">Voornaam:</label>
                                <input type="text" class="form-control" name="voornaam" />
                            </div>
                            <div class="form-group">
                                <label for="tussenvoegsel">Tussenvoegsel:</label>
                                <input type="text" class="form-control" name="tussenvoegsel" />
                            </div>
                            <div class="form-group">
                                <label for="achternaam">Achternaam:</label>
                                <input type="text" class="form-control" name="achternaam" />
                            </div>
                            <div class="form-group">
                                <label form="email">E-mail adres:</label>
                                <input type="text" class="form-control" name="email" />
                            </div>
                            <div class="form-group">
                                <label for="klantnummer"><b>Klantnummer:</b></label>
                                <input type="text" class="form-control" name="klantnummer" />
                            </div>
                            <div class="form-group">
                                <label for="bedrijfsnaam"><b>Bedrijfsnaam:</b></label>
                                <input type="text" class="form-control" name="bedrijfsnaam" />
                            </div>
                            <button type="submit" class="btn btn-primary">Toevoegen</button>
                        </form>
                    </div>
                </div>
                @endsection