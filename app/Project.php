<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $fillable = [
    'projectnaam',
    'opdracht_gever',
    'status',
    'deadline',
    'omschrijving',
    'start_fase_1',
    'einde_fase_1',
    'start_fase_2',
    'einde_fase_2',
    'start_fase_3',
    'einde_fase_3',
  ];
}
