<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projecten = Project::all();

        return view('projecten.index', compact('projecten'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projecten.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
      $request->validate([
        'projectnaam'=>'',
        'status'=>'',
        'opdracht_gever'=> '',
        'deadline'=> '',
        'omschrijving'=> '',
        'start_fase_1' => '',
        'einde_fase_1' => '',
        'start_fase_2' => '',
        'einde_fase_2' => '',
        'start_fase_3' => '',
        'einde_fase_3' => '',

      ]);

      $project = new Project($request->all());

      $project->save();

      return redirect('/projecten')->with('Hoppaah!', 'Project is toegevoegd in de database');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $project = Project::find($id);

      return view('projecten.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return view('projecten.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'projectnaam'=>'',
        'status'=>'',
        'opdracht_gever'=> '',
        'opdracht_gever'=> '',
        'deadline'=> '',
        'start_fase_1' => '',
        'einde_fase_1' => '',
        'start_fase_2' => '',
        'einde_fase_2' => '',
        'start_fase_3' => '',
        'einde_fase_3' => '',
        
      ]);

      $project = Project::find($id);
      $project->projectnaam = $request->get('projectnaam');
      $project->opdracht_gever = $request->get('opdracht_gever');
      $project->deadline = $request->get('deadline');
      $project->status = $request->get('status');
      $project->omschrijving = $request->get('omschrijving');
      $project->start_fase_1 = $request->get('start_fase_1');
      $project->einde_fase_1 = $request->get('einde_fase_1');
      $project->start_fase_2 = $request->get('start_fase_2');
      $project->einde_fase_2 = $request->get('einde_fase_2');
      $project->start_fase_3 = $request->get('start_fase_3');
      $project->einde_fase_3 = $request->get('einde_fase_3');
      $project->save();

      return redirect('/projecten')->with('Wham!', 'De klus is bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();

      return redirect('/projecten')->with('Gelukt,', 'project is verwijderd');
    }
}
