<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;

class ShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shares = Share::all();

        return view('shares.index', compact('shares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request);
      $request->validate([
        'voornaam'=>'',
        'tussenvoegsel'=> '',
        'achternaam' => '',
        'email' => '',
        'klantnummer' => '',
        'bedrijfsnaam' => ''

      ]);
      $share = new Share([
        'voornaam' => $request->get('voornaam'),
        'tussenvoegsel'=> $request->get('tussenvoegsel'),
        'achternaam'=> $request->get('achternaam'),
        'email'=> $request->get('email'),
        'klantnummer'=> $request->get('klantnummer'),
        'bedrijfsnaam'=> $request->get('bedrijfsnaam')
      ]);
      $share->save();
      return redirect('/shares')->with('Hoppaah!', 'Klant is toegevoegd in de database');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $share = Share::find($id);

      return view('shares.show', compact('share'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $share = Share::find($id);

        return view('shares.edit', compact('share'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'voornaam'=>'',
        'tussenvoegsel'=> '',
        'achternaam' => '',
        'email' => '',
        'klantnummer' => '',
        'bedrijfsnaam' => ''
        
      ]);

      $share = Share::find($id);
      $share->voornaam = $request->get('voornaam');
      $share->tussenvoegsel = $request->get('tussenvoegsel');
      $share->achternaam = $request->get('achternaam');
      $share->email = $request->get('email');
      $share->klantnummer = $request->get('klantnummer');
      $share->bedrijfsnaam = $request->get('bedrijfsnaam');
      $share->save();

      return redirect('/shares')->with('Wham!', 'De klant is bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $share = Share::find($id);
        $share->delete();

      return redirect('/shares')->with('Helaas,', 'er is een klant ontsnapt');
    }
}
