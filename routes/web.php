<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dhevak', function () {
    return view('dhevak');
});

Route::get('/trello', function () {
    return view('trello');
});

Route::get('/home', function () {
    return view('home');
});

Route::resource('shares', 'ShareController');
Route::resource('projecten', 'ProjectController');


